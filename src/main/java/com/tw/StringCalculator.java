package com.tw;

public class StringCalculator {
    public int add(String string) {
		String[] filtered = string.split("\\D+");
		int result = 0;
		for (String s : filtered) {
			if (!s.equals(""))
				result += Integer.parseInt(s);
		}
        return result;
    }
}
